
        SetFactory("OpenCASCADE");

        length = 3; height = 1.5; t_w = 0.02;
        t_r = 0.02; l_r = 0.5000000000000001; dist = 0.3333333333333333;

        Box(1) = {0, 0, 0, length, t_w, height};

        Box(2) = {0.0-t_r/2, 0, 0, t_r, l_r, height}; 
Box(3) = {0.3333333333333333-t_r/2, 0, 0, t_r, l_r, height}; 
Box(4) = {0.6666666666666666-t_r/2, 0, 0, t_r, l_r, height}; 
Box(5) = {1.0-t_r/2, 0, 0, t_r, l_r, height}; 
Box(6) = {1.3333333333333333-t_r/2, 0, 0, t_r, l_r, height}; 
Box(7) = {1.6666666666666665-t_r/2, 0, 0, t_r, l_r, height}; 
Box(8) = {2.0-t_r/2, 0, 0, t_r, l_r, height}; 
Box(9) = {2.333333333333333-t_r/2, 0, 0, t_r, l_r, height}; 
Box(10) = {2.6666666666666665-t_r/2, 0, 0, t_r, l_r, height}; 
Box(11) = {3.0-t_r/2, 0, 0, t_r, l_r, height}; 

vol[] = BooleanUnion { Volume{1}; Delete; } { Volume{2}; Volume{3}; Volume{4}; Volume{5}; Volume{6}; Volume{7}; Volume{8}; Volume{9}; Volume{10}; Volume{11};  Delete; };
        
        Printf('%g', vol[0]);

        Physical Volume("Volume") = {vol[0]};
        Physical Surface("front") = {1};
        Physical Surface("bottom") = {2};
        Physical Point("fixpoint") = {1};

        Mesh.MeshSizeMin = 0.02;
        Mesh.MeshSizeMax = 0.2;
        Mesh.SaveAll = 1;
        Mesh.ElementOrder = 2;

        Mesh 3;
        Save "wall_opt_22.msh";