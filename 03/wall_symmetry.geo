
SetFactory("OpenCASCADE");

length = 3; height = 1; t_w = 0.05;
t_r = 0.05; l_r = 0.8; dist = 0.5; d_sym = 0.5;

Box(1) = {0, 0, 0, length, t_w, height};
Box(2) = {-t_w/2, 0, 0, t_w, -d_sym, height};
Box(3) = {length-t_w/2, 0, 0, t_w, -d_sym, height};
BooleanUnion { Volume{1}; Delete; } { Volume{2}; Volume{3}; Delete; };

v_rip_1 = newv;
Box(v_rip_1+0) = {0.0-t_r/2, 0, 0, t_r, l_r, height}; 
Box(v_rip_1+1) = {0.5-t_r/2, 0, 0, t_r, l_r, height}; 
Box(v_rip_1+2) = {1.0-t_r/2, 0, 0, t_r, l_r, height}; 
Box(v_rip_1+3) = {1.5-t_r/2, 0, 0, t_r, l_r, height}; 
Box(v_rip_1+4) = {2.0-t_r/2, 0, 0, t_r, l_r, height}; 
Box(v_rip_1+5) = {2.5-t_r/2, 0, 0, t_r, l_r, height}; 
Box(v_rip_1+6) = {3.0-t_r/2, 0, 0, t_r, l_r, height}; 

vol[] = BooleanUnion { Volume{1}; Delete; } { Volume{v_rip_1+0}; Volume{v_rip_1+1}; Volume{v_rip_1+2}; Volume{v_rip_1+3}; Volume{v_rip_1+4}; Volume{v_rip_1+5}; Volume{v_rip_1+6};  Delete; };

Printf('%g', vol[0]);

Physical Volume("Volume") = {vol[0]};
Physical Surface("inside") = {6, 7, 31};
Physical Surface("bottom") = {9};
Physical Point("fixpoint") = {42};
Physical Surface("Symmetry") = {8, 34};

Mesh.MeshSizeMin = 0.02;
Mesh.MeshSizeMax = 0.1;
Mesh.SaveAll = 1;
Mesh.ElementOrder = 2;

Mesh 3;

Save "wall_symmetry.msh";
