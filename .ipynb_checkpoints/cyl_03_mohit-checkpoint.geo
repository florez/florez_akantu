//+
SetFactory("OpenCASCADE");
//+
Cylinder(1) = {0, 0, 0, 0, 0, 1, 0.5, 2*Pi};
//+
//Surface Loop(2) = {1, 2, 3};
//+
//Volume(2) = {2};

Physical Surface("bottom") = {3};
Physical Surface("top") = {2};
Physical Volume("steel") = {1};