//+
SetFactory("OpenCASCADE");
//+
t = 0.05;
Circle(1) = {0, -0, 0, 0.5, 0, 2*Pi};
Circle(2) = {0, -0, 0, 0.5+t, 0, 2*Pi};
//+
Extrude {0, 0, 5} {
  Curve{1}; 
}

Extrude {0, 0, 5} {
  Curve{2}; 
}
//+
Physical Surface("Inside") = {1};
//+

Physical Curve("top") = {1};
//+
Physical Point("fixpoint") = {3,4};
//+

//+
Curve Loop(3) = {6};
//+
Curve Loop(4) = {4};
//+
Plane Surface(3) = {3, 4};
Physical Surface("bottom") = {3};
//+
Curve Loop(5) = {2};
//+
Curve Loop(6) = {1};
Plane Surface(4) = {5, 6};
//+
Surface Loop(1) = {3, 2, 4, 1};
//+
Volume(1) = {1};
