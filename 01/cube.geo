SetFactory("OpenCASCADE");

Box(1) = {0, 0, 0, 1, 1, 1};

Physical Surface("bottom") = {5};
Physical Surface("top") = {6};
Physical Volume("cube") = {1};

box1[] = Point{1};
Printf("this is text %g", box1[2]);

Mesh.MeshSizeMin = 1;
Mesh.MeshSizeMax = 1;
Mesh.SaveAll = 1;
Mesh.ElementOrder = 1;

Mesh 3;

Save "cube.msh";
