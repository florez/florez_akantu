
SetFactory("OpenCASCADE");

ra = 0.5; ri = 0.45; h = 1;
Cylinder(1) = {0, 0, 0, 0, 0, h, ra, 2*Pi};
Cylinder(2) = {0, 0, 0, 0, 0, h, ri, 2*Pi};

vol[] = BooleanDifference{ Volume{1}; Delete; }{ Volume{2}; Delete; };

Physical Volume("body") = {vol[0]};
Physical Surface("inside") = {4};
Physical Surface("bottom") = {7};
Physical Point("fixpoint") = {6};

Mesh.MeshSizeMin = 0.05;
Mesh.MeshSizeMax = 0.1;
Mesh.SaveAll = 1;
Mesh.ElementOrder = 2;

// Mesh 3;

Save "hollow_cylinder.msh";
