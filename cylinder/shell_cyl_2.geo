//+
SetFactory("OpenCASCADE");
Circle(1) = {0, 0, 0, 0.5, 0, 2*Pi};
Line Loop(1) = {1};
//+
Extrude {0, 0, 1} {
  Curve{1}; 
}
//+
Physical Line("bottom") = {1};
//+
Physical Line("top") = {3};
//+
Physical Surface("surface") = {1};
//+
Physical Curve("height_curve") = {2};
