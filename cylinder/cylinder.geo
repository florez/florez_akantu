Merge "cylinder.stp";
SetFactory("Built-in");
//+
Recombine Surface {4, 1};
//+
Recombine Surface {2, 1, 4, 3};
//+
Recombine Surface {2, 1, 4, 3};
//+
Recombine Surface {2, 1, 4, 3};
//+
MeshSize {3, 1, 4, 2} = 0.1;
//+
MeshSize {3, 1, 4, 2} = 0.05;
//+
MeshSize {3, 1, 4, 2} = 2;
//+
MeshSize {3, 1, 4, 2} = 0.001;
//+
Curve Loop(7) = {4};
//+
Plane Surface(5) = {7};
//+
Physical Surface("Inside") = {4};
//+
MeshSize {1, 3, 2, 4} = 0.1;
//+
MeshSize {1, 3, 2, 4} = 1;
//+
MeshSize {1, 3, 2, 4} = 10;
//+
MeshSize {1, 3, 2, 4} = 1000;
//+
MeshSize {1, 3, 2, 4} = 500;
//+
MeshSize {1, 3, 2, 4} = 500;
//+
MeshSize {1, 3, 2, 4} = 200;
//+
MeshSize {1, 3, 2, 4} = 50;
//+
MeshSize {1, 3, 4, 2} = 20;
//+
MeshSize {1, 3, 2, 4} = 500;
