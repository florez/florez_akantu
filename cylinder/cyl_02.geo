
SetFactory("OpenCASCADE");
Circle(1) = {0,0,0,0.5,0,Pi}; //Point 1, 2
Circle(2) = {0, 0, 1, 0.5, 0, Pi}; //Point 3, 4
Circle(3) = {0,0,0,0.5, Pi, 2*Pi}; //Point 1, 2
Circle(4) = {0, 0, 1, 0.5, Pi, 2*Pi}; //Point 3, 4
Line(5) = {1, 3};
Line(6) = {2, 4};
Line(7) = {1, 2};
Line(8) = {3, 4};
Line Loop(9) = {1, 6, -2, -5};
Line Loop(10) = {3, 5, -4, -6};
Curve Loop(11) = {1, 7};
Curve Loop(12) = {2, 8};
Curve Loop(13) = {3, 7};
Curve Loop(14) = {4, 8};
Curve Loop(15) = {5, 8, -6, -7};

Plane Surface(1) = {11};
Plane Surface(2) = {12};
// Plane Surface(100) = {13};
// Plane Surface(101) = {14};
Plane Surface(102) = {15};

Surface(3) = {9};
// Surface(4) = {10};
// Surface Loop(5) = {4, 101, 102, 100};
Surface Loop(6) = {3, 1, 102, 2};
Physical Surface("bottom") = {1};
Physical Surface("top") = {2};

// Volume(1) = {5};
Volume(2) = {6};
Physical Volume("volume") = {2};




