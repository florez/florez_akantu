SetFactory("OpenCASCADE");

Point(1) = {0, 0, 0};
Point(2) = {0, .5, 0};
Circle(1) = {0,0,0,0.5,0,2*Pi};
Extrude {0, 0, 1} {
  Curve{1}; 
}
//+
Curve Loop(2) = {3};
//+
Plane Surface(2) = {2};

Curve Loop(3) = {1};
Plane Surface(3) = {3};

Physical Surface("bottom") = {3};
Physical Surface("top") = {2};
Physical Surface("surface") = {1};
